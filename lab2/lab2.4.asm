
		.data 0x10010000

var1: 	.word 0x62		# define var1
var2:   .word 0x47		# define var2		

		.extern ex1 4		# define ex1 with external label
		.extern ex2 4		# define ex2 with external label

		.text
		.globl main

main:	addu $s0, $ra, $0	# save $31 in $16
		li $t0, 7
		move $t1, $t0

		lw $t2, var1	# save var1 value in register
		sw $t2, ex1		# store var1 into ex1 address

		lw $t4, var2	# save var2 value in register
		sw $t4, ex2		# store var2 into ex2 address

# restore now the return address in $ra and return from main
		addu $ra, $0, $s0 	# return address back in $31
		jr $ra 		# return from main


