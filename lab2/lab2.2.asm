
		.data 0x10010000
var1: 	.word 0x83		
var2: 	.word 0x104
var3:	.word 0x111
var4:	.word 0x119

first:	.byte 'y'
last:	.byte 'q'


		.text
		.globl main

main:	addu $s0, $ra, $0	# save $31 in $16

		la $t2, var1	# get var1 address
		la $t3, var4	# get var4 address

		lw $t4, var1	# temparily save var1 value in register $t4
		lw $t5, var4	# temparily save var4 value in register $t5
		
		sw $t4, 0($t3)	# store original var4 into var1 address
		sw $t5, 0($t2)	# store original var1 into var4 address

		la $t3, var2	# get var2 address
		la $t2, var3	# get var3 address

		lw $t8, var2	# temparily save var2 value in register $t8
		lw $t0, var3	# temparily save var3 value in register $t0
		
		sw $t8, 0($t2)	# store original var3 into var2 address
		sw $t0, 0($t3)	# store original var2 into var3 address

        la $t6, first	# get first address
		la $t7, last	# get last address
# restore now the return address in $ra and return from main
		addu $ra, $0, $s0 	# return address back in $31
		jr $ra 		# return from main


