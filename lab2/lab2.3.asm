
	.data 0x10010000
var1: 	.word 0x0083 		
var2: 	.word 0x0104
var3:	.word 0x0111
var4:	.word 0x0119

first:	.byte 'y'
last:	.byte 'q'


		.text
		.globl main

main:		 addu $16, $31, $0        # save $31 in $16 

             lui $10, 4097 [var1]     # get var1 address 
             lui $1, 4097 [var4]      # get var4 address 
             ori $11, $1, 12 [var4]   
			 
             lui $1, 4097             # temparily save var1 value in register $t4 
             lw $12, 0($1)       
			 
             lui $1, 4097             # temparily save var4 value in register $t5 
             lw $13, 12($1)         
			 
             sw $12, 0($11)           # store original var4 into var1 address 
             sw $13, 0($10)           # store original var1 into var4 address 
			 
             lui $1, 4097 [var2]      # get var2 address 
             ori $11, $1, 4 [var2]   
			 
             lui $1, 4097 [var3]      # get var3 address 
             ori $10, $1, 8 [var3]    
             lui $1, 4097             # temparily save var2 value in register $t8 
             lw $24, 4($1)            
			 
             lui $1, 4097             # temparily save var3 value in register $t0 
             lw $8, 8($1)             
			 
             sw $24, 0($10)           # store original var3 into var2 address 
             sw $8, 0($11)            # store original var2 into var3 address 
			 
             lui $1, 4097 [first]     # get first address 
             ori $14, $1, 16 [first]  
			 
             lui $1, 4097 [last]      # get last address 
             ori $15, $1, 17 [last]   

# restore now the return address in $ra and return from main
		addu $31, $0, $16  	# return address back in $31
		jr $31 		# return from main


