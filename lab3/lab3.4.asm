.data
my_array: .space 40            # reserve 40 bytes (10 words) for the array
initial_value: .word 6

.text
.globl main
main:
	lw $t1, initial_value($0) # $t1(j) <- initial_value
	la $t0, my_array          # $t0 <- start address of array
	li $t3, 0                 #initial index i = 0
	li $t4, 10                #set index range 10
	
loop:
	ble $t4, $t3 exit         # index i > 10 goto exit 
	addi $t3, $t3,1           # i++
	sw $t1, 0($t0)            # my_array[i] = j
	addi $t1, $t1, 1          # j++
	addi $t0, $t0, 4          # calculate address of next element
	j loop
	
exit:
	jr $ra

