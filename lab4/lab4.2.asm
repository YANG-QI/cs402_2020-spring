.text
.globl main
main:
    addi $sp, $sp, -4      # substract 4 from stack pointer
    sw $ra, 4($sp)         # save $ra in allocated stack space
    jal test
    nop
    lw $ra, 4($sp)         # restore $ra from stack
    addi $sp, $sp, 4       # free stack space
    jr $ra

test:
    nop
    jr $ra
