#1(a)
dat <- read.csv("http://www.cs.iit.edu/~virgil/cs470/varia/traces/benchmarks/spice.din", header=FALSE, sep="")
spice.address<-strtoi(dat$V2,16L)
hist(spice.address,xlim=range(spice.address), col="purple")

dat2 <- read.csv("http://www.cs.iit.edu/~virgil/cs470/varia/traces/benchmarks/cc1.din", header=FALSE, sep=" ")
cc1.address<-strtoi(dat2$V2,16L)
hist(cc1.address,col="red")


#1(b)
table(unlist(dat$V1))
#The result of the code shown below
#The frequency of writes is 66538.
#The frequency of reads is 150699.


table(unlist(dat2$V1))
#The result of the code shown below
#The frequency of writes is 83030.
#The frequency of reads is 159631