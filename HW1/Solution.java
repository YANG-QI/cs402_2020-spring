package hw1.a;

import java.util.Arrays;
import java.util.Random;

public class Solution {
    public static int[][] matrixMultiply(int[][] m1, int[][] m2) {	
        int r = m1.length;
        int c = m2[0].length;
        int[][] result = new int[r][c];
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                int temp = 0;
                for (int k = 0; k < m1[0].length; k++) {
                    int i1 = m1[i][k];
                    int i2 = m2[k][j];
                    temp += i1 * i2;
                }
                result[i][j] = temp;
            }
        }
        return result;
    }

    public static int[][] randomMatrix(int r, int c) {
        Random random = new Random();
        int[][] result = new int[r][c];
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                result[i][j] = random.nextInt(1000);
            }
        }
        return result;
    }
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        int times = 10;
        for (int i = 0; i < times; i++) {
            int[][] intM1 = randomMatrix(1000, 600);
            int[][] intM2 = randomMatrix(600, 700);
            matrixMultiply(intM1, intM2);
        }
        long end = System.currentTimeMillis();
        double cost = (end - start);
        System.out.println("Integer matrix multiply: size (1000x600)x(600x700)");
        System.out.println("average time: " + cost / times + "ms");
        System.out.println();
    }
}

